## The API Gateway

This bracket management software consists of 2 main parts: accounts and series. Accounts can be either head TOs or volunteer TOs. Series (think Genesis) contain tournaments (Genesis 1, 2, 3, etc.), and tournaments contain events (singles, doubles).

The API Gateway's purpose is to handle all incoming requests from the front end and transform and orchestrate those requests to the other services, megaservice and account service.
